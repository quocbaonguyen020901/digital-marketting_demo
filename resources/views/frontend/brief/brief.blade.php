@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/content/content.css?v=' . time()) }}">
<link rel="stylesheet" href="{{ ('assets/pr&booking/pr&booking.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example all-services">
        <div class="container">
            <div class="title_all-services">
                <h3>Bạn hãy chọn Brief (bản yêu cầu dịch vụ) phù hợp nhất với nhu <br>cầu của mình và điền đầy đủ thông tin.</h3>
                <p>Chúng tôi sẽ nhận được thông tin của bạn ngay lập tức! </p>
            </div>
            <div class="items_all-services">
                <div class="example_all-services row">
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                </div>
                <div class="example_all-services row">
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                </div>
                <div class="example_all-services row">
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <h3>Digital Marketing</h3>
                            <p>Digital Marketing là chiến lược dùng Internet làm phương tiện cho các hoạt động marketing và trao đổi thông tin
                            </p>
                            <a href="#">Brief Về Digital Marketing</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

