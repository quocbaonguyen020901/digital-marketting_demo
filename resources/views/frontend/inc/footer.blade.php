<section class="example">
        <div class="container">
            <div class="banner-email">
                <div id="banner-title__email">
                    <h4>Cập nhật xu hướng</h4>
                    <h3>TRUYỀN THÔNG QUẢNG CÁO ĐA KÊNH</h3>
                    <p>
                        Đừng bỏ lỡ thông tin mới nhất về ngành truyền thông quảng cáo,
                        đón đầu xu thế bạn nhé!
                    </p>
                    <div id="email">
                        <input type="text" placeholder="Địa chỉ email của bạn là " />
                        <button id="get-news">Nhận tin tức</button>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="main-footer">
                    <div id="item__footer">
                        <img src="./img/group-578.png" alt="" />
                        <div id="text__footer">
                            <p>
                                Mocha Ads - Hệ thống dịch vụ quảng cáo truyền thông do Viettel
                                Media triển khai.
                            </p>
                        </div>
                        <div id="icon__footer">
                            <img src="./img/fb.png" alt="#" />
                            <img src="./img/yt.png" alt="#" />
                            <img src="./img/insta.png" alt="#" />
                        </div>
                    </div>
                    <div id="item__footer">
                        <h3 class="title__about-us">Về chúng tôi</h3>
                        <ul>
                            <li><a href="/page/brief.html">Brief</a></li>
                            <li><a href="#" class="port">Portfolio</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div id="item__footer">
                        <h3 class="title__about-us">Dịch vụ</h3>
                        <div class="list_options">
                            <ul>
                                <li><a href="#">Facebook Ads</a></li>
                                <li><a href="#">Google Ads</a></li>
                                <li><a href="#">Tiktok Ads</a></li>
                                <li><a href="#">KOLs/Influencer</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="item__footer">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="./img/dmca.png" alt="#" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="./img/thong-bao-website-voi-bo-cong-thuong_grande .png" alt="#" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="./img/Google-Premier-Partner.png" alt="#" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="./img/facebook-marketing-partner-logo-B7C40FB59C-seeklogo.png" alt="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="item__footer " class="footer__consult">
                        <div class="item__footer--consult">
                            <h4 id="title-services">Tư vấn miễn phí</h4>
                            <p>
                                Tư vấn MIỄN PHÍ chiến dịch truyền thông & quảng cáo về sản
                                phẩm của bạn?
                            </p>
                            <div id="footer--consult__contact">
                                <img class="image" src="./img/Group 86.png" alt="#" />
                                <div id="text">
                                    <p>Liên hệ với chúng tôi</p>
                                    <a href="#">Ấn để gửi mail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact">
                    <div class="contact-form">
                        <div class="contact-form__infor-page">
                            <p>
                                Cơ quan chủ quản: Công ty Truyền thông Viettel (Viettel Media)
                                – Chi nhánh Tập đoàn Công nghiệp – Viễn thông Quân đội.
                            </p>
                            <p>
                                Trụ sở: Tầng 4, Tòa nhà The Light (CT2 Trung Văn), đường Tố
                                Hữu, Nam Từ Liêm, Hà Nội
                            </p>
                            <p>
                                Giấy phép: Số 217/GXN-TTĐT do Cục QL Phát Thanh Truyền Hình và
                                TTĐT cấp ngày 28/12/2011
                            </p>
                            <p>Chịu trách nhiệm nội dung: Ông Võ Thanh Hải</p>
                            <p>
                                Chăm sóc khách hàng: 198 (Miễn phí) | Quảng
                                cáo:hoptac@viettel.com.vn
                            </p>
                        </div>
                        <div class="contact-form__ads">
                            <div class="contact-form__ads-left">
                                <p>©2020, Viettel Media. All Rights Reserved</p>
                            </div>
                            <div class="contact-form__ads-right">
                                <p class="ads"><a href="#">Term of services</a></p>
                                <p class="ads"><a href="#">Pivacy policy</a></p>
                                <p class="ads"><a href="#">Legal Info</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
    integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
    integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
</script>
<script src="./main.js"></script>

</html>