@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/content/content.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example banner">
        <div class="container">
            <div class="example-banner_side">
                <div class="side-items_left">
                    <div class="title-items">
                        <h3 class="content-slogant">Creative Content</h3>
                        <p class="content-sentence">Cách thức khám phá nội dung thông minh của TikTok cho phép người dùng tìm được nội dung mới dựa trên sở thích và thói quen xem của họ,</p>
                        <p class="content-sentence">giúp các thương hiệu tăng trưởng nhanh và tối đa.</p>
                    </div>
                </div>
                <div class="side-items_right">
                    <div class="img-items">
                        <img src="/img/image 53.png" alt="#">
                        <div class="img-items__press">
                            <img src="/img/Group.png" alt="#">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example procedure">
        <div class="container">
            <div class="example-procedure_side">
                <div class="title-procedure">
                    <h3 class="content-procedure">
                        Quy trình tạo ra sản phẩm
                    </h3>  
                </div>
                <div class="items-procedure">
                    <div class="items-produre__step">
                        <img src="/img/Group 452.png" alt="#">
                        <h3 class="content-procedure">Tìm hiểu</h3>
                        <p class="detail__step">Trao đổi với khách hàng (Client) về mục tiêu kỳ vọng và tìm hiểu các thông tin liên quan để đưa ra giải pháp</p>
                    </div>
                    <div class="items-produre__step">
                        <img src="/img/Group 452.png" alt="#">
                        <h3 class="content-procedure">Lập kế hoạch</h3>
                        <p class="detail__step">Tiến hành ký kết hợp đồng và timeline triển khai sơ bộ</p>
                    </div>
                    <div class="items-produre__step">
                        <img src="/img/Group 452.png" alt="#">
                        <h3 class="content-procedure">Ký hợp đồng</h3>
                        <p class="detail__step">Lên kế hoạch triển khai chi tiết cho khách hàng (Client)</p>
                    </div>
                    <div class="items-produre__step">
                        <img src="/img/Group 452.png" alt="#">
                        <h3 class="content-procedure">Sản phẩm</h3>
                        <p class="detail__step">Triển khai và xuất bản nội dung theo kế hoạch đã đề ra</p>
                    </div>
                    <div class="items-produre__step">
                        <img src="/img/Group 452.png" alt="#">
                        <h3 class="content-procedure">Báo cáo</h3>
                        <p class="detail__step">Báo cáo và nghiệm thu kết quả của quá trình sản xuất nội dụng sáng tạo</p>
                    </div>
                </div>
                <div class="items-project">
                    <h3 class="content-project">
                        Các dự án đã triển khai
                    </h3>
                    <div class="project-icon_items">
                       <div class="content-top">
                            <div class="project_1 col-lg-3">
                                <img src="/img/Group 148.png" alt="#">
                            </div>
                            <div class="project_2 col-lg-3">
                                <img src="/img/coca.png" alt="#">
                            </div>
                            <div class="project_3 col-lg-3">
                                <img src="/img/pepsi.png" alt="#">
                            </div>
                            <div class="project_4 col-lg-3">
                                <img src="/img/logo_xanh copy.png" alt="#">
                            </div>
                       </div>
                       <div class="content-bottom">
                            <div class="project_5 col-lg-3 ">
                                <img src="/img/lazada.png" alt="#">
                            </div>
                            <div class="project_6 col-lg-3 ">
                                <img src="/img/lotte.png" alt="#">
                            </div>
                            <div class="project_7  col-lg-3">
                                <img src="/img/Group 299.png" alt="#">
                            </div>
                            <div class="project_8 col-lg-3 ">
                                <img src="/img/shoppe.png" alt="#">
                            </div>
                       </div>
                       <div class="btn-read-more">
                        <button class="find-out-more">
                            <span>Tìm hiểu thêm</span> 
                        </button>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example achievement">
        <div class="container">
            <div class="items-achievement">
                <h3 class="title_achievement">
                    Thành tựu của chúng tôi
                </h3>
                <p class="detail_achievement">Đây là điều Viettel Media tự hào khi những clip TVC hay Clip viral thực hiện cho các nhãn hàng đã được đông <br> đảo cộng đồng mạng đón nhận và tương tác</p>
            </div>
            <div class="comment_parameters">
                <div class="col-4">
                    <h4>12 Tr</h4>
                    <span>Like</span>
                </div>
                <div class="col-4">
                    <h4>6.3 Tr</h4>
                    <span>Bình luận</span>
                </div>
                <div class="col-4">
                    <h4>4 Tr</h4>
                    <span>Chia sẻ</span>
                </div>
            </div>
            <div class="image_achievement">
                <img src="/img/image 22.png" alt="#">
                <p>Sau 1 ngày lên sóng, MV quảng cáo của JustaTee x ViettelPay lọt vào Top Trending trên <br> Youtube với vị trí 40 cùng 600 nghìn lượt xem.</p>
            </div>
            <div>
                <input type="radio" name="page" checked>
                <input type="radio" name="page">
                <input type="radio" name="page">
                <input type="radio" name="page">
                <input type="radio" name="page">
                <input type="radio" name="page">
                <input type="radio" name="page">
            </div>
        </div>
    </section>
    <section class="example other-services">
        <div class="container">
            <div class="title_services">
                Lựa chọn  dịch vụ phù hợp cho doanh nghiệp của bạn
            </div>
            <div class="table-services">
                <div class="types-of-services">
                    <div class="item_services">
                        <span>Bài Viết</span>
                    </div>
                    <div class="item_services">
                        <span>Bài Dịch</span>
                    </div>
                    <div class="item_services"> 
                        <span>Bài Pr</span>
                    </div>
                </div>
                <div class="detail-services">   
                    <div class="col-4">
                        <div class="items_title-services">
                            <h4>Bài viết mô tả sản phẩm </h4>
                            <p>Tối ưu hóa mô tả sản phẩm (product description) là yếu tố rất quan trọng giúp tăng hiệu quả của các chiến dịch chạy quảng cáo Facebook hay Google Shopping.</p>
                        </div>
                        <div class="quantity">
                            <h4 class="title_quantity">
                                Số từ
                            </h4>
                            <p>Bao nhiêu bài viết thì phù hợp với bạn?</p>
                            <div class="input-options">
                                <span class="input-optisons_1"> 1 bài</span>
                            </div>
                            <div class="input-options option_2">
                                <span class="input-optisons_1">5 bài</span>
                                <span class="input-optisons_2">+800.000 đ</span>
                            </div>
                        </div>
                        <div class="option-quantity">
                            <div class="title_option">Tùy chọn số lượng</div>
                            <div class="item_option-quantity">
                                <div class="toggle-pill">
                                  <input type="checkbox" id="pill1" name="check">
                                  <label for="pill1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="up-down_option">
                            <div class="minus">-</div>
                            <div class="numbers">1</div>
                            <div class="plus">+</div>
                        </div>
                        <div class="quantity">
                            <h4 class="title_quantity">
                                Số từ
                            </h4>
                            <p>Bao nhiêu bài viết thì phù hợp với bạn?</p>
                            <div class="input-options">
                                <span class="input-optisons_1"> 1 bài</span>
                            </div>
                            <div class="input-options option_2">
                                <span class="input-optisons_1">1000 - 1200 bài</span>
                                <span class="input-optisons_2">+800.000 đ</span>
                            </div>
                        </div>
                        <div class="price-services">
                            <h5>200.000 đ</h5>
                        </div>
                        <div class="buy-services">
                            <button>Đăng ký ngay</button>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="items_title-services">
                            <h4>Bài viết mô tả sản phẩm </h4>
                            <p>Tối ưu hóa mô tả sản phẩm (product description) là yếu tố rất quan trọng giúp tăng hiệu quả của các chiến dịch chạy quảng cáo Facebook hay Google Shopping.</p>
                        </div>
                        <div class="quantity">
                            <h4 class="title_quantity">
                                Số từ
                            </h4>
                            <p>Bao nhiêu bài viết thì phù hợp với bạn?</p>
                            <div class="input-options">
                                <span class="input-optisons_1"> 1 bài</span>
                            </div>
                            <div class="input-options option_2">
                                <span class="input-optisons_1">5 bài</span>
                                <span class="input-optisons_2">+800.000 đ</span>
                            </div>
                        </div>
                        <div class="option-quantity">
                            <div class="title_option">Tùy chọn số lượng</div>
                            <div class="item_option-quantity">
                                <div class="toggle-pill">
                                  <input type="checkbox" id="pill1" name="check">
                                  <label for="pill1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="up-down_option">
                            <div class="minus">-</div>
                            <div class="numbers">1</div>
                            <div class="plus">+</div>
                        </div>
                        <div class="quantity">
                            <h4 class="title_quantity">
                                Số từ
                            </h4>
                            <p>Bao nhiêu bài viết thì phù hợp với bạn?</p>
                            <div class="input-options">
                                <span class="input-optisons_1"> 1 bài</span>
                            </div>
                            <div class="input-options option_2">
                                <span class="input-optisons_1">1000 - 1200 bài</span>
                                <span class="input-optisons_2">+800.000 đ</span>
                            </div>
                        </div>
                        <div class="price-services">
                            <h5>200.000 đ</h5>
                        </div>
                        <div class="buy-services">
                            <button>Đăng ký ngay</button>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="items_title-services">
                            <h4>Bài viết mô tả sản phẩm </h4>
                            <p>Tối ưu hóa mô tả sản phẩm (product description) là yếu tố rất quan trọng giúp tăng hiệu quả của các chiến dịch chạy quảng cáo Facebook hay Google Shopping.</p>
                        </div>
                        <div class="quantity">
                            <h4 class="title_quantity">
                                Số từ
                            </h4>
                            <p>Bao nhiêu bài viết thì phù hợp với bạn?</p>
                            <div class="input-options">
                                <span class="input-optisons_1"> 1 bài</span>
                            </div>
                            <div class="input-options option_2">
                                <span class="input-optisons_1">5 bài</span>
                                <span class="input-optisons_2">+800.000 đ</span>
                            </div>
                        </div>
                        <div class="option-quantity">
                            <div class="title_option">Tùy chọn số lượng</div>
                            <div class="item_option-quantity">
                                <div class="toggle-pill">
                                  <input type="checkbox" id="pill1" name="check">
                                  <label for="pill1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="up-down_option">
                            <div class="minus">-</div>
                            <div class="numbers">1</div>
                            <div class="plus">+</div>
                        </div>
                        <div class="quantity">
                            <h4 class="title_quantity">
                                Số từ
                            </h4>
                            <p>Bao nhiêu bài viết thì phù hợp với bạn?</p>
                            <div class="input-options">
                                <span class="input-optisons_1"> 1 bài</span>
                            </div>
                            <div class="input-options option_2">
                                <span class="input-optisons_1">1000 - 1200 bài</span>
                                <span class="input-optisons_2">+800.000 đ</span>
                            </div>
                        </div>
                        <div class="price-services">
                            <h5>200.000 đ</h5>
                        </div>
                        <div class="buy-services">
                            <button>Đăng ký ngay</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection