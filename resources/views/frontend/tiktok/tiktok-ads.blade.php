@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/tiktok/tiktok-ads.css?v=' . time()) }}">


@endsection

@section('content') 
    <section class="example banner">
        <div class="container">
            <div class="example-banner_side">
                <div class="side-items_left">
                    <div class="title-items">
                        <h3 class="content-slogant">Kể câu chuyện thương hiệu của bạn theo cách độc đáo với TikTok</h3>
                        <p class="content-sentence">Cách thức khám phá nội dung thông minh của TikTok cho phép người dùng tìm được nội dung mới dựa trên sở thích và thói quen xem của họ, giúp các thương hiệu tăng trưởng nhanh và tối đa.</p>
                    </div>
                </div>
                <div class="side-items_right">
                    <div class="img-items">
                        <img src="/img/Group 455.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example review">
        <div class="container">
            <div class="title-review">
                <h3>Phát triển việc kinh doanh của bạn <br>
                    với các giải pháp thương hiệu đa dạng trên TikTok</h3>
            </div>
            <div class="example-banner_side">
                <div class="side-items_left">
                    <div class="title-items">
                        <h3 class="content-slogant" id="slogant">Quảng cáo hiển thị ngay khi khởi động ứng dụng</h3>
                        <span class="items-small">#Brand Takeover</span>
                        <p class="content-sentence" id="">Ngay lập tức thu hút sự chú ý của người dùng bằng toàn màn hình tĩnh hoặc động, đem đến tác động trực quan cho thương hiệu của bạn.</p>
                        <div class="option-items_1">
                            <li>Thu hút sự chú ý của người dùng thông qua trải nghiệm hình ảnh toàn màn hình.</li>
                            <li>Có nhiều tùy chọn định dạng sáng tạo với màn hình tĩnh hoặc động.</li>
                            <li>Hỗ trợ cả JPG 3 giây và video 3-5 giây.</li>
                        </div>
                    </div>
                </div>
                <div class="side-items_right">
                    <div class="img-items">
                        <img src="/img/Group 455.png" alt="">
                    </div>
                </div>
            </div>
            <div class="example-banner_side">
                <div class="side-items_left">
                    <div class="img-items">
                        <img src="/img/Group 455.png" alt="">
                    </div>
                </div>
                <div class="side-items_right">
                    <div class="title-items">
                        <h3 class="content-slogant" id="slogant">Quảng cáo trên dòng thời gian</h3>
                        <span class="items-small">#In-Feed Ads</span>
                        <p class="content-sentence" id="">Kể câu chuyện thương hiệu của bạn như một người sáng tạo của TikTok bằng cách tích hợp nội dung video vào dòng thời gian “For You” của người dùng.</p>
                        <div class="option-items_1">
                            <li>Xuất hiện trong dòng nội dung “For You” với video tối đa 60 giây được phát tự động với âm thanh.</li>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <section class="example services">
        <div class="title-review">
            <h3>Tại Mocha Ads bạn có cần</h3>
            <p class="items__review">Đối tác cấp cao của Google tại Việt Nam <br>
                Tư vấn loại hình quảng cáo phù hợp chiến dịch/ sản phẩm dịch vụ của khách hàng
            </p>
        </div>
        <div class="container mocha-ads">
            <div class="col-sm-4">
                <h4>100%<span>KPI</span></h4>
                <p>Triển khai và tối ưu chiến dịch quảng cáo </p>
            </div>
            <div class="col-sm-4">
                <h4 >#10</h4>
                <p>Digital Marketing Agency 
                    uy tín hàng đầu tại Việt Nam</p>
            </div>
            <div class="col-sm-4">
                <h4>3000+</h4>
                <p>Dự án về quảng cáo Google thuộc các lĩnh vực du lịch, thể thao, hàng tiêu dùng, viễn thông,…</p>
            </div>
        </div>
    </section>
    <section class="example services">
        <div class="title-review">
            <h3>Lựa chọn dịch vụ <br> phù hợp cho doanh nghiệp của bạn</h3>
        </div>
        <div class="container table ">
         <table>
            <tbody>
                <tr>
                    <td width="33%"></td>
                    <td width="33%" class="text-table" id="th">Thuê tài khoản</td>
                    <td width="33%" class="text-table" id="th">Quản lý tài khoản</td>
                </tr>
                <tr>
                    <td class="text-table">Tư vấn chiến dịch quảng cáo</td>
                    <td class="check" ><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Lên kế hoạch phân bổ ngân sách</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Triển khai và tối ưu quảng cáo</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Báo cáo chiến dịch</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Hỗ trợ 24/7</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Nhân viên hỗ trọ riêng</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td></td>
                    <td id="th"><button class="text-table basic" >Đăng ký ngay</button></td>
                    <td id="th"><button class="text-table premium" >Đăng ký ngay</button></td>
                </tr>
            </tbody>
         </table>
        </div>
    </section>
@endsection