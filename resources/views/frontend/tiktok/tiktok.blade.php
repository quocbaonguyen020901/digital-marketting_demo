@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/tiktok/tiktok.css?v=' . time()) }}">
<link rel="stylesheet" href="{{ ('assets/tiktok/tiktok-ads.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example banner-tiktok">
        <div class="container">
            <div class="content-target">
                <div class="detail-target">
                    <div class="item-target">
                        <h3>Mọi mục tiêu marketing 
                            đều nằm trong tầm tay bạn.</h3>
                            <p>Chúng tôi cung cấp các giải pháp quảng cáo cho mọi trình độ chuyên môn. </p>
                            <p>Hơn 2 tỷ người sử dụng Facebook mỗi tháng. Vì vậy, bạn sẽ tìm thấy mọi loại đối tượng mình muốn tiếp cận ở đây.</p>
                    </div>
                </div>
                <div class="image-target">
                    <img src="/img/image-408.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="example ads-services">
        <div class="container">
            <div class="ads-services_title">
                <h3>Các loại dịch vụ quảng cáo trên Facebook</h3>
                <p>Mỗi mục tiêu cụ thể sẽ có phù hợp với một hoặc một vài hình thức quảng cáo tương ứng</p>
            </div>
            <div class="types-of-advertising">
                <div class="content-interact">
                    <div class="interact_items">
                        <div class="interact_detail">
                            <div class="interact_detail-items">
                                <h3>Quảng cáo tương tác </h3>
                                <span class="tag">#Page Post Engagement</span>
                                <p>Tăng tương tác cho bài viết, gia tăng tỷ lệ chuyển đổi đối với các bài viết liên quan tới sale trực tiếp</p>
                            </div>
                        </div>
                        <div class="interact_image">
                            <img src="/img/iphone 3.png" alt="#">
                        </div>
                    </div>
                </div>
                <div class="content-video">    
                    <div class="video_items">
                        <div class="video_image">
                            <img src="/img/iphone 3.png" alt="">
                        </div>
                        <div class="video_detail">
                            <div class="video_detail-items">   
                                <h3>Quảng cáo Video</h3>
                                <span class="tag">#Video View</span>
                                <p>Nâng cao nhận thức về thương hiệu thông qua video với thông điệp cụ thể</p>
                                <p>Text mô tả vd: Tropicana đã sử dụng quảng cáo video với mục tiêu mức độ nhận biết thương hiệu.</p>
                            </div> 
                        </div>     
                    </div>
                </div>
                <div class="content-message">
                    <div class="message_items">
                        <div class="message_detail">
                            <div class="message_detail-items">
                                <h3>Quảng cáo tin nhắn</h3>
                                <span class="tag">#Messenger (Sponsor)</span>
                                <p>Tiếp cận khách hàng qua cách gửi tin nhắn tài trợ hoặc quảng cáo trong messenger / bảng 
                                tin</p>
                            </div>
                        </div>
                        <div class=message_image>
                            <div class="image_1">
                                <img src="/img/Group 445.png" alt="#">
                            </div>
                            <div class="image_2">
                                <img src="/img/Group 446.png" alt="#">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-approach">
                    <div class="approach-items">
                        <div class="approach_image">
                            <img src="/img/Group 395.png" alt="#">
                        </div>
                        <div class="approach_detail">
                            <div class="approach_detail-items">
                                <h3>Quảng lượt tiếp cận</h3>
                                <span class="tag">#Reach</span>
                                <p>Nâng cao nhận thức về thương hiệu thông qua việc đưa các thông điệp tiếp cận tới tệp khách hàng tiềm năng.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-potential-customers">
                    <div class="potential-customers_items">
                        <div class="potential_detail">
                            <div class="potential_detail-items">
                                <h3>Quảng cáo 
                                    tìm kiếm khách hàng tiềm năng</h3>
                                <span class="tag">#lead</span>
                                <p>Quảng cáo tới tệp khách hàng tiềm năng và điền thông tin khách hàng theo mẫu được cấu hình sẵn.</p>
                            </div>
                        </div>
                        <div class="potential_image">
                            <img src="/img/iphone 3.png" alt="#">
                        </div>
                    </div>
                </div>
                <div class="content-like-page">
                    <div class="content-like_items">
                        <div class="content-like_image">
                            <img src="/img/iphone 3.png" alt="#">
                        </div>
                        <div class="content-like_detail">
                            <div class="content-like_detail-items">
                                <h3>Quảng cáo tăng like fanpage</h3>
                                <span class="tag">#Page Likes</span>
                                <p>Tăng mức độ uy tín của Fanpage và tạo ra cộng đồng đông đảo, đúng với chân dung khách hàng mà đối tác mong muốn.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example google__services container">
        <div class="title-ads">
            <h2>Tại Mocha Ads bạn có cần</h2>
            <p>Đối tác cấp cao của Google tại Việt Nam <br>
                Tư vấn loại hình quảng cáo phù hợp chiến dịch/ sản phẩm dịch vụ của khách hàng
            </p>
        </div>
        <div class="container mocha-ads row">
            <div class="col-sm-4">
                <h4>100%<span>KPI</span></h4>
                <p>Triển khai và tối ưu chiến dịch quảng cáo </p>
            </div>
            <div class="col-sm-4 ">
                <h4 >#10</h4>
                <p>Digital Marketing Agency 
                    uy tín hàng đầu tại Việt Nam</p>
            </div>
            <div  class="col-sm-4 ">
                <h4>3000+</h4>
                <p>Dự án về quảng cáo Google thuộc các lĩnh vực du lịch, thể thao, hàng tiêu dùng, viễn thông,…</p>
            </div>
        </div>
    </section>
    <section class="example services">
        <div class="title-review">
            <h3>Lựa chọn dịch vụ <br> phù hợp cho doanh nghiệp của bạn</h3>
        </div>
        <div class="container table ">
         <table>
            <tbody>
                <tr>
                    <td width="33%"></td>
                    <td width="33%" class="text-table" id="th">Thuê tài khoản</td>
                    <td width="33%" class="text-table" id="th">Quản lý tài khoản</td>
                </tr>
                <tr>
                    <td class="text-table">Tư vấn chiến dịch quảng cáo</td>
                    <td class="check" ><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Lên kế hoạch phân bổ ngân sách</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Triển khai và tối ưu quảng cáo</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Báo cáo chiến dịch</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Hỗ trợ 24/7</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Nhân viên hỗ trọ riêng</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td></td>
                    <td id="th"><button class="text-table basic" >Đăng ký ngay</button></td>
                    <td id="th"><button class="text-table premium" >Đăng ký ngay</button></td>
                </tr>
            </tbody>
         </table>
        </div>
    </section>

@endsection