@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/content/content.css?v=' . time()) }}">
<link rel="stylesheet" href="{{ ('assets/kol/kol.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example banner">
        <div class="container">
            <div class="example-banner_side">
                <div class="side-items_left">
                    <div class="title-items">
                        <p class="content-sentence">Bạn cần KOLs, Influencers quảng bá sản phẩm dịch vụ của mình. Nhưng chưa biết <span>NÊN CHỌN AI?</span></p>
                        <div class="button-price">
                            <button class="button_items">
                                <p>Liên hệ tư vấn</p>
                            </button>  
                            <button class="price-list">
                                <p >Bảng giá</p>
                            </button>
                        </div>
                    </div>   
                </div>
                <div class="side-items_right">
                    <div class="img-items">
                        <img src="/img/Group 568.png" alt="#">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example banner_2">
        <div class="container">
            <div class="example-banner_side-2">
                <div class="side-items_left">
                    <div class="image_banner-2">
                        <img src="/img/image 61.png" alt="#">
                    </div>
                </div>
                <div class="side-items-right">
                    <div class="title_items-2">
                        <h3 class="title_banner-2">Key Opinion Leader</h3>
                        <p class="content-sentence-2">
                            Sở hữu rất nhiều chuyên gia có tiếng ở đa lĩnh vực, Viettel Media tin rằng sẽ đáp ứng được mọi yêu cầu của khách hàng khi cần đội ngũ chuyên gia tư vấn và quảng bá sản phẩm/dịch vụ của mình.
                        </p>
                        <button class="button-advise">
                            <span>Tư vấn KoLs</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example banner_3">
        <div class="container">
            <div class="example-banner_side-3">
                <div class="side-items_left">
                    <div class="title_items-3">
                        Influencer
                    </div>
                    <p class="content-sentence-3">
                        Sở hữu và quản lý danh sách các Influencer có sức ảnh hưởng trong cộng đồng mạng (Facebook, Youtube, Tiktok,...) với lượng Fans hùng hậu từ 50.000 - 5.000.000 followers. Chắc chắn sẽ nơi lý tưởng để khách hàng (Client) gửi gắm thông điệp truyền tải tới tệp đối tượng của mình.
                    </p>
                    <button class="button-advise">
                        <span>Tư vấn Influencers</span>
                    </button>
                </div>
                <div class="side-items_right">
                    <div class="image_banner-3">
                        <img src="/img/Rectangle 545.png" alt="#">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example Kol-representative" >
        <div class="container">
            <div class="exemple_kol-representative">
                <div class="title-representative">
                    <h3>KOL/Influencer tiêu biểu </h3>
                </div>
                <div class="image-representative">
                    <div class="kol_items-content row">
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/tg.png" alt="#">
                            <span>Trường Giang</span>
                        </div>
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/tt.png" alt="#">
                            <span>Trấn Thành</span>
                        </div>
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/dp.png" alt="#">
                            <span>Đức Phúc</span>
                        </div>
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/erik.png" alt="#">
                            <span>Erik</span>
                        </div>
                    </div>
                    <div class="kol_items-content row">
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/virus.png" alt="#">
                            <span>Virus</span>
                        </div>
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/domoxi.png" alt="#">
                            <span>Độ mixi</span>
                        </div>
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/pew.png" alt="#">
                            <span>Pew Pew</span>
                        </div>
                        <div class="items-kol col-lg-3 col-md-6">
                            <img src="/img/hmizi.png" alt="#">
                            <span>Hoà Minzy</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example procedure">
        <div class="container">
            <div class="example-procedure_side">
                <div class="items-project">
                    <h3 class="content-project">
                        Các dự án đã triển khai
                    </h3>
                    <div class="project-icon_items">
                    <div class="content-top">
                            <div class="project_1 col-lg-3">
                                <img src="/img/Group 148.png" alt="#">
                            </div>
                            <div class="project_2 col-lg-3">
                                <img src="/img/coca.png" alt="#">
                            </div>
                            <div class="project_3 col-lg-3">
                                <img src="/img/pepsi.png" alt="#">
                            </div>
                            <div class="project_4 col-lg-3">
                                <img src="/img/logo_xanh copy.png" alt="#">
                            </div>
                    </div>
                    <div class="content-bottom">
                            <div class="project_5 col-lg-3">
                                <img src="/img/lazada.png" alt="#">
                            </div>
                            <div class="project_6 col-lg-3">
                                <img src="/img/lotte.png" alt="#">
                            </div>
                            <div class="project_7 col-lg-3">
                                <img src="/img/Group 299.png" alt="#">
                            </div>
                            <div class="project_8 col-lg-3">
                                <img src="/img/shoppe.png" alt="#">
                            </div>
                    </div>
                    <div class="btn-read-more">
                            <button class="find-out-more">
                                <span>Tìm hiểu thêm</span> 
                            </button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example price-services_kol">
        <div class="container">
            <div class="price-services_form">
                <div class="form_price-left"> 
                    <div class="title_price-form">
                        <h3>Bảng giá dịch vụ Kols/Influencer</h3>
                        <p>Tuỳ vào Kols/Influencer mà có mức giá khác nhau</p>
                        <p>vì vậy, để đảm bảo cập nhật thông tin mới nhất về bảng giác dịch vụ, hãy liên hệ ngày với Mocha Ads để nhận báo giá và tư vấn nhanh nhất</p>
                    </div>
                </div>
                <div class="form_price-right">
                    <div class="form_information">
                        <div class="items_form-infor">
                            <label for="name">Họ và tên</label>
                            <input type="text" placeholder="Họ và tên của bạn là">
                        </div>
                        <div class="items_form-infor flex-form ">
                            <div class="items-flex">
                                <label for="email">Địa chỉ email</label>
                                <input type="text" name= "email" placeholder="Địa chỉ Email của bạn là">
                            </div>
                            <div class="items-flex">
                                <label for="phone" >Số điện thoại</label>
                                <input type="text" name="phone" placeholder="Số điện thoại của bạn là">
                            </div>
                        </div>
                        <div class="items_form-infor">
                            <label for="detial-request">Chi tiết yêu cầu</label>
                            <input placeholder="Hãy viết gì đó ..."></input>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
