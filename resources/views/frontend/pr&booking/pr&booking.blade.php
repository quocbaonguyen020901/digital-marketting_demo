@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/content/content.css?v=' . time()) }}">
<link rel="stylesheet" href="{{ ('assets/pr&booking/pr&booking.css?v=' . time()) }}">

@endsection

@section('content') 
    <section class="example banner">
        <div class="container">
            <div class="example-banner_side">
                <div class="side-items_left">
                    <div class="title-items">
                        <h3><span>#10</span> Đơn vị uy tín triển khai bài viết PR và book báo chí</h3>
                        <p class="content-sentence">Triển khai các bài PR cho các đối tác lớn như: Unilever, Shopee, cocacola, pepsi, vpbank lazada, mocha, xgaming. 
                        </p>
                        <p class="content-sentence">Tư vấn lựa chọn báo chí phù hợp với chiến dịch của nhãn hàng (99,99%). </p>
                        <p class="content-sentence">89% mức độ người quan tâm tới nhãn hàng khi triển khai.</p>

                    </div>   
                </div>
                <div class="side-items_right">
                    <div class="img-items">
                        <img src="/img/ghep2.png" alt="#">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example banner banner_pr">
        <div class="container">
            <div class="example-banner_side">
                <div class="side-items_right">
                    <div class="img-items">
                        <img src="/img/thgnguoi.png" alt="#">
                    </div>
                </div>
                <div class="side-items_left">
                    <div class="title-items">
                        <h3>Đặt banner quảng cáo trên các trang báo điện tử</h3>
                        <p class="content-sentence">Sở hữu và là đối tác chiến lược của các trang tin điện tử hot nhất 2020 <span>(Tiin, Netnews, Dantri, Vnexpress, 24h, nguoiduatin, tuoitre,...)</span> với lưu lượng KHỦNG sẽ giúp thông điệp của Khách hàng (Client) tới gần hơn với tệp đối tượng mục tiêu
                        </p>
                        <p class="content-sentence"><span>50</span> Trang báo quản lý
                        </p>
                        <p class="content-sentence"><span>30</span>  Trang báo là đối tác chiến lược</p>
                    </div>   
                </div>
                
            </div>
        </div>
    </section>
    <section class="example resources">
        <div class="container">
            <div class="example-resources-side">
                <h3>Tài nguyên của chúng tôi</h3>
                <div class="example-resources-items">
                    <div class="items_resources">
                            <div class="content-resources-top">
                                <div class="items-brand ">
                                    <div class="image-brand">
                                        <img src="/img/brand4.png" alt=""#>
                                    </div>
                                    <div class="detail-brand">
                                        <h4>Tiin</h4>
                                        <p>Traffic</p>
                                        <p class="down">3.73Tr</p>
                                    </div>
                                </div>
                                <div class="items-brand ">
                                    <div class="image-brand">
                                        <img src="/img/brand3.png" alt=""#>
                                    </div>
                                    <div class="detail-brand">
                                        <h4>Netnews</h4>
                                        <p>Traffic</p>
                                        <p class="down">1.06Tr</p>
                                    </div>
                                </div>
                                <div class="items-brand  ">
                                    <div class="image-brand">
                                        <img src="/img/brand5.png" alt=""#>
                                    </div>
                                    <div class="detail-brand ">
                                        <h4>Mocha</h4>
                                        <p>Traffic</p>
                                        <p class="down">1.06Tr</p>
                                    </div>
                                </div>
                                <div class="items-brand ">
                                    <div class="image-brand">
                                        <img src="/img/brand1.png" alt=""#>
                                    </div>
                                    <div class="detail-brand">
                                        <h4>Keeng Movies</h4>
                                        <p>Traffic</p>
                                        <p class="down">3.5Tr</p>
                                    </div>
                                </div>
                                <div class="items-brand  ">
                                    <div class="image-brand">
                                        <img src="/img/brand1.png" alt=""#>
                                    </div>
                                    <div class="detail-brand">
                                        <h4>Keeng Music</h4>
                                        <p>Traffic</p>
                                        <p class="down">2.5Tr</p>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="content-resources-bottom">
                                
                            </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example procedure">
        <div class="container">
            <div class="example-procedure_side">
                <div class="items-project">
                    <h3 class="content-project">
                        Các dự án đã triển khai
                    </h3>
                    <div class="project-icon_items">
                       <div class="content-top">
                            <div class="project_1 col-lg-3">
                                <img src="/img/Group 148.png" alt="#">
                            </div>
                            <div class="project_2 col-lg-3">
                                <img src="/img/coca.png" alt="#">
                            </div>
                            <div class="project_3 col-lg-3">
                                <img src="/img/pepsi.png" alt="#">
                            </div>
                            <div class="project_4 col-lg-3">
                                <img src="/img/logo_xanh copy.png" alt="#">
                            </div>
                       </div>
                       <div class="content-bottom">
                            <div class="project_5 col-lg-3">
                                <img src="/img/lazada.png" alt="#">
                            </div>
                            <div class="project_6 col-lg-3">
                                <img src="/img/lotte.png" alt="#">
                            </div>
                            <div class="project_7 col-lg-3">
                                <img src="/img/Group 299.png" alt="#">
                            </div>
                            <div class="project_8 col-lg-3">
                                <img src="/img/shoppe.png" alt="#">
                            </div>
                       </div>
                       <div class="btn-read-more">
                            <button class="find-out-more">
                                <span>Tìm hiểu thêm</span> 
                            </button>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example other-services">
        <div class="container">
            <div class="title_services">
                Lựa chọn  dịch vụ <br>phù hợp cho doanh nghiệp của bạn
            </div>
            <div class="content-services_TVC">
                <div class="content-services_left">
                    <div class="title_services-content">
                        <h5>HÌNH THỨC</h5>
                        <h4>Booking PR</h4>
                    </div>
                    <div class="funtions_services-content">
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Đăng bài trên Tiin / Netnews</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Hỗ trợ chỉnh sửa (tối đa 2 lần)</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;2 liên kết</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Hỗ trợ tư vấn nội dung bài viết</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Nhân viên hỗ trợ riêng</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Hỗ trợ 24/7</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Nhân viên hỗ trợ riêng</p>
                    </div>
                    <div class="line-services"></div>
                    <div class="price-services_site">
                        <span>8.888.888đ</span>
                    </div>
                    <div class="accept-now">
                        <span>Đăng ký ngay</span>
                    </div>
                </div>
                <div class="content-services_right">
                    <div class="title_services-content">
                        <h5>HÌNH THỨ<Caption></Caption></h5>
                        <h4>Banner Quảng cáo</h4>
                    </div>
                    <div class="funtions_services-content">
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Đặt banner trên các trang/app: Tiin.vn, Netnews</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Tư vấn vị trí đặt banner hiệu quả phù hợp với chiến dịch</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Chiết khấu khi mua gói lần 2</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Hỗ trợ 24/7</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Nhân viên hỗ trợ riêng</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Hỗ trợ 24/7</p>
                        <p><img src="/img/Vector.png" alt="#">&nbsp;Nhân viên hỗ trợ riêng</p>
                    </div>
                    <div class="line-services"></div>
                    <div class="price-services_site">
                        <span>8.888.888đ</span>
                    </div>
                    <div class="accept-now">
                        <span>Đăng ký ngay</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
