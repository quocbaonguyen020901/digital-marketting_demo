@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/blog/blog.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example blog-tutorial">
        <div class="container">
          <div class="blog-turotial_content">
            <div class="content-blog_left">
              <div class="image-blog">
                <img src="/img/Rectangle 684.png" alt="#">
              </div>
              <div class="text-blog">
                <span>Hướng dẫn</span>
                <a href="/page/blog-post.html">
                  <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                </a>
                <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                <p>7 phút để đọc</p>
              </div>
            </div>
            <div class="content-blog_right row">
                <div class="col-lg-4">
                    <div class="items-image">
                        <img src="/img/Rectangle 685.png" alt="#">
                    </div>
                    <div class="items-text">
                        <span>SEO</span>
                        <h3>Digital Marketing là làm gì? Top những kỹ năng...</h3>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="items-image">
                        <img src="/img/Rectangle 686.png" alt="#">
                    </div>
                    <div class="items-text">
                        <span>SEO</span>
                        <h3>Digital Marketing là làm gì? Top những kỹ năng...</h3>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="items-image">
                        <img src="/img/Rectangle 687.png" alt="#">
                    </div>
                    <div class="items-text">
                        <span>SEO</span>
                        <h3>Digital Marketing là làm gì? Top những kỹ năng...</h3>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </section>
    <section class="example blog-main-other">
        <div class="container">
            <div class="other-blog">
                <div class="search_input">
                    <span class="form_control-feedback"><img src="/img/search.png" alt="#"></span>
                    <input type="text" class="form_control" placeholder="Tìm kiếm ">
                    <button class="btn_search">
                        <span>Tìm kiếm</span>
                    </button>
                </div>
                <div class="content-blog_items">
                    <div class="image-blog">
                      <img src="/img/Rectangle 688.png" alt="#">
                    </div>
                    <div class="text-blog">
                      <span>Hướng dẫn</span>
                      <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                      <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                      <p class="read_time">7 phút để đọc</p>
                    </div>
                </div>
                <div class="content-blog_items">
                    <div class="image-blog">
                      <img src="/img/Rectangle 689.png" alt="#">
                    </div>
                    <div class="text-blog">
                      <span>Hướng dẫn</span>
                      <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                      <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                      <p>7 phút để đọc</p>
                    </div>
                </div>
                <div class="content-blog_items">
                    <div class="image-blog">
                      <img src="/img/Rectangle 690.png" alt="#">
                    </div>
                    <div class="text-blog">
                      <span>Hướng dẫn</span>
                      <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                      <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                      <p>7 phút để đọc</p>
                    </div>
                </div>
                <div class="content-blog_items">
                    <div class="image-blog">
                      <img src="/img/Rectangle 691.png" alt="#">
                    </div>
                    <div class="text-blog">
                      <span>Hướng dẫn</span>
                      <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                      <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                      <p>7 phút để đọc</p>
                    </div>
                </div>
                <div class="content-blog_items">
                    <div class="image-blog">
                      <img src="/img/Rectangle 692.png" alt="#">
                    </div>
                    <div class="text-blog">
                      <span>Hướng dẫn</span>
                      <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                      <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                      <p>7 phút để đọc</p>
                    </div>
                </div>
                <div class="content-blog_items">
                    <div class="image-blog">
                      <img src="/img/Rectangle 693.png" alt="#">
                    </div>
                    <div class="text-blog">
                      <span>Hướng dẫn</span>
                      <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết </h3>
                      <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu ...</p>
                      <p>7 phút để đọc</p>
                    </div>
                </div>
            </div>
            <div class="most-view">
                <div class="title_blog-recent">
                    <h3>Xem Nhiều Nhất</h3>
                </div>
                <div class="items_blog-recent">
                    <div class="number_blog">
                        <p>01</p>
                    </div>
                    <div class="content_blog">
                      <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                      <span>5 phút để đọc</span>
                    </div>
                </div>
                <div class="items_blog-recent">
                  <div class="number_blog">
                      <p>02</p>
                  </div>
                  <div class="content_blog">
                    <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                    <span>5 phút để đọc</span>
                  </div>
                </div>
                <div class="items_blog-recent">
                  <div class="number_blog">
                      <p>03</p>
                  </div>
                  <div class="content_blog">
                    <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                    <span>5 phút để đọc</span>
                  </div>
                </div>
                <div class="items_blog-recent">
                <div class="number_blog">
                    <p>04</p>
                </div>
                <div class="content_blog">
                  <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                  <span>5 phút để đọc</span>
                </div>
                </div>
                <div class="items_blog-recent">
            <div class="number_blog">
                <p>05</p>
            </div>
            <div class="content_blog">
              <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
              <span>5 phút để đọc</span>
            </div>
                </div>
            </div>
        </div>
    </section>
    <section class="exmaple pagination_site">
        <div class="container">
          <div class="pagination">
            <a href="#" class="back">Quay lại</a>
            <a href="#" class="checked" >1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#" class="next">Tiếp tục</a>
          </div>
        </div>
    </section>
@endsection