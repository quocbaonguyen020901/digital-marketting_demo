@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/google/google.css?v=' . time()) }}">

@endsection

@section('content')
<section class="example plant-dev">
    <div class="container">
        <div class="content-left">
            <div class="content-left__main">
                <div class="title">
                    <h3 class="title__google-ads">Phát triển doanh nghiệp của bạn thông qua
                        Google Ads</h3>
                </div>
                <div class="content__detail">
                    <p>Hiển thị quảng cáo cho khách hàng khi họ đang tìm kiếm các doanh nghiệp giống như doanh nghiệp
                        của bạn trên Google Tìm kiếm và Maps. </p>
                    <p>Bạn chỉ trả tiền khi quảng cáo mang lại kết quả, chẳng hạn như khi khách hàng nhấp vào quảng cáo
                        để tới trang web hoặc khách hàng gọi đến doanh nghiệp của bạn.
                    </p>
                </div>
            </div>
        </div>
        <div class="content-right">
            <div class="content-right__img">
                <img src="/img/Group 345.png" alt="#">
            </div>
        </div>
    </div>
</section>
<section class="example-google__services">
    <div class="container">
        <div class="title-ads">
            <h2>Các loại dịch vụ quảng cáo trên Google</h2>
            <p>Mỗi mục tiêu cụ thể sẽ có phù hợp với một hoặc một vài hình thức quảng cáo tương ứng</p>
        </div>
        <div class="content__search-ads">
            <div class="content-left">
                <div class="content-left__search-detail">
                    <h3>Quảng cáo tìm kiếm</h3>
                    <p>Quảng cáo trên mạng tìm kiếm của Google (Google Search)</p>
                    <p>Dựa trên việc người dùng truy vấn tìm kiếm (từ khóa) trên Google từ đó tiếp cận người dùng thông
                        qua quảng cáo.</p>
                </div>
            </div>
            <div class="content-right">
                <div class="content-right__img-s">
                    <img src="/img/Group 366.png" alt="#">
                </div>
            </div>
        </div>
        <div class="content__search-ads">
            <div class="content-right">
                <div class="content-right__img-search">
                    <img src="/img/Group 377.png" alt="#">
                </div>
            </div>
            <div class="content-left">
                <div class="content-left__search-detail">
                    <h3>Quảng cáo hiển thị</h3>
                    <p>Quảng cáo trên mạng hiển thị của Google (Google Display Network). </p>
                    <p>Bạn có thể lựa chọn hiển thị trên các trang web thuộc mạng hiển thị hoặc cũng có thể sử dụng các
                        tiêu chí khác như từ khóa, chủ đề,…</p>
                </div>
            </div>
        </div>
        <div class="content__search-ads">
            <div class="content-left">
                <div class="content-left__search-detail">
                    <h3>Quảng cáo mua sắm</h3>
                    <p>Tương thích với các trang thương mại điện tử hoặc đáp ứng được các yếu tố cơ bản của trang thương
                        mại điện tử (giỏ hàng, thanh toán, các chính sách thuế phí, vận chuyển, hoàn hủy, bảo mật,…)</p>
                </div>
            </div>
            <div class="content-right">
                <div class="content-right__img-s">
                    <img src="/img/Group 378.png" alt="#">
                </div>
            </div>
        </div>
        <div class="content__search-ads">
            <div class="content-right">
                <div class="content-right__img-s">
                    <img src="/img/Group 385.png" alt="#">
                </div>
            </div>
            <div class="content-left">
                <div class="content-left__search-detail">
                    <h3>Quảng cáo Video</h3>
                    <p>Quảng cáo sản phẩm dịch vụ của bạn trên Youtube hoặc đối tác video của Google</p>
                    <div class="item__google-ads">
                        <div class="text_item">
                            <h3>Youtube Trueview</h3>
                            <p>Quảng cáo trong luồng có thể bỏ qua</p>
                        </div>
                    </div>
                    <div class="item__google-ads">
                        <div class="text_item">
                            <h3>Youtube Bumper</h3>
                            <p>Thời lượng tối đa 15s</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="example-google__services">
    <div class="title-ads">
        <h2>Tại Mocha Ads bạn có cần</h2>
        <p>Đối tác cấp cao của Google tại Việt Nam <br>
            Tư vấn loại hình quảng cáo phù hợp chiến dịch/ sản phẩm dịch vụ của khách hàng
        </p>
    </div>
    <div class="container mocha-ads row">
        <div class="col-sm-4">
            <!-- <img class="mocha-img" src="/img/Group 387.png" alt="#"> -->
            <h4>100%<span>KPI</span></h4>
            <p>Triển khai và tối ưu chiến dịch quảng cáo </p>
        </div>
        <div class="col-sm-4">
            <h4>#10</h4>
            <p>Digital Marketing Agency
                uy tín hàng đầu tại Việt Nam</p>
        </div>
        <div class="col-sm-4">
            <!-- <img class="mocha-img" src="/img/3000+.png" alt="#"> -->
            <h4>3000+</h4>
            <p>Dự án về quảng cáo Google thuộc các lĩnh vực du lịch, thể thao, hàng tiêu dùng, viễn thông,…</p>
        </div>
    </div>
</section>
<section class="example-google__services container">
    <div class="title-ads">
        <h2>Lựa chọn dịch vụ <br> phù hợp cho doanh nghiệp của bạn</h2>
    </div>
    <div class="container table">
        <table>
            <tbody>
                <tr>
                    <td width="33%"></td>
                    <td width="33%" class="text-table" id="th">Thuê tài khoản</td>
                    <td width="33%" class="text-table" id="th">Quản lý tài khoản</td>
                </tr>
                <tr>
                    <td class="text-table">Tư vấn chiến dịch quảng cáo</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Lên kế hoạch phân bổ ngân sách</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Triển khai và tối ưu quảng cáo</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Báo cáo chiến dịch</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Hỗ trợ 24/7</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td class="text-table">Nhân viên hỗ trọ riêng</td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                    <td class="check"><i class="fa-sharp fa-solid fa-check"></i></td>
                </tr>
                <tr>
                    <td></td>
                    <td id="th"><button class="text-table basic">Đăng ký ngay</button></td>
                    <td id="th"><button class="text-table premium">Đăng ký ngay</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
@endsection


