@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/blog-post/blog-post.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example blog-post">
        <div class="container">
            <div class="content-user_blog">
                <div class="site-items">
                    <div class="item_user-image">
                        <img src="/img/user.png" alt="#">
                    </div>
                    <div class="item_user-name">
                        <span class="name_user">Nguyễn Hoàng quân</span>
                        <p class="date_up">8 thg 7, 2020</p>
                    </div>
                    <div class="items_position">
                        <span>Manager</span>
                    </div>
                    <div class="icon_share">

                    </div>
                </div>
                <div class="content-blog_site">
                    <div class="title_blog-site">
                        <h3>Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến</h3>
                    </div>
                    <div class="detail_blog-site">
                        <p>Digital Marketing là một mảng nhỏ hơn của Marketing, yêu cầu những kiến thức chuyên sâu hơn các chiến lược marketing căn bản và khả năng sáng tạo.</p>
                    </div>
                </div>
            </div>
            <div class="content-blog_right">
                <div class="col-4">
                    <div class="items-image">
                        <img src="/img/Rectangle 685.png" alt="#">
                    </div>
                    <div class="items-text">
                        <span>SEO</span>
                        <h3>Digital Marketing là làm gì? Top những kỹ năng...</h3>
                    </div>
                </div>
                <div class="col-4">
                    <div class="items-image">
                        <img src="/img/Rectangle 686.png" alt="#">
                    </div>
                    <div class="items-text">
                        <span>SEO</span>
                        <h3>Digital Marketing là làm gì? Top những kỹ năng...</h3>
                    </div>
                </div>
                <div class="col-4">
                    <div class="items-image">
                        <img src="/img/Rectangle 687.png" alt="#">
                    </div>
                    <div class="items-text">
                        <span>SEO</span>
                        <h3>Digital Marketing là làm gì? Top những kỹ năng...</h3>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <section class="example content-post">
        <div class="container">
            <div class="content-post_site">
                <p>Nếu bạn đang cân nhắc thử sức với ngành Digital Marketing thì hãy cùng tìm hiểu những kỹ năng cần có và công việc của một digital marketer là gì nhé.</p>
                <p>Digital Marketer là ai?
                <br>        
                Thông thường một digital marketer sẽ liên quan đến việc sử dụng các kênh digital để tạo ra leads và xây dựng brand awareness (nhận diện thương hiệu).</p>
                <p>Các kênh digital thường gặp gồm có: </p>
                <p>
                Website của công ty
                <br>
                Social media 
                <br>
                Bảng xếp hạng công cụ tìm kiếm (SEM)
                <br>
                Email marketing
                <br>
                Quảng cáo online
                <br>
                Blog của công ty 
                <br>
                Dựa vào đây, digital marketer phải sử dụng các công cụ đo lường để tìm ra điểm yếu và phương án cải thiện hiệu suất trên các kênh này. Tùy công ty, digital marketer có thể chịu trách nhiệm cho toàn bộ chiến lược digital của công ty hoặc chỉ tập trung vào một thứ.</p>
                <p>Tại các công ty SMEs hoặc start-up thường có một chuyên gia hoặc một quản lý chung; trong khi đó ở các tập đoàn, trách nhiệm này có thể được phân bổ cho một team hoặc thậm chí cho nhiều bộ phận khác nhau có liên quan.</p>
                <p>Đó là một lĩnh vực nơi kinh nghiệm hoặc thực tập tốt (internship) nói lên nhiều điều. “Certifications are the new degree,” một khẳng định nữa từ Justin Emig. Có rất nhiều chứng chỉ online bạn có thể thêm vào CV, tăng độ tin cậy với nhà tuyển dụng. Đây thường là cách tốt nhất để thu hút sự chú ý của nhà tuyển dụng nếu bạn muốn một công việc trong ngành digital marketing. Một điểm lưu ý nhỏ: Đừng cố gắng đề cập tất cả “thành tích” của bạn trên nền tảng online như là các trang mạng xã hội nghìn followers.</p>
                <p>
                    Top kỹ năng và hiểu biết cần có của Digital Marketer
                    <br>
                    1. Video 
                    <br>
                    Có một thống kê chỉ ra rằng “Trong thời đại mà người dùng bị bủa vây bởi quá nhiều thông tin, mức độ tập trung của chúng ta giảm xuống khoảng 8,25 giây”. Vì vậy để thu hút được sự chú ý của người dùng online chưa bao giờ là chuyện dễ dàng. Các nghiên cứu đã cho thấy video giúp tăng lượng tương tác và tạo xếp hạng cao hơn trên Google. Bạn không cần trở thành video producer nhưng bạn có thể học cách tạo ra một video cơ bản. Hiểu cách viết kịch bản, sử dụng các nền tảng và apps để tạo ra video và các yếu tố ảnh hưởng của video sẽ là điểm cộng lớn cho bạn khi ứng tuyển cho công việc digital marketing.</p>
                <p> 2. SEO & SEM 
                    <br>
                    Tìm kiếm online điều hướng quảng cáo digital. Bạn phải hiểu về tối ưu hóa công cụ tìm kiếm (SEO) và SEM (search engine marketing) nếu muốn làm việc trong ngành này. Bạn đừng cảm thấy quá lo lắng về sử dụng back-end. Hiểu về tầm quan trọng của SEO và cách ứng dụng của nó trong ngành còn quan trọng hơn rất nhiều. Đây là bước đầu tiên cảu bất cứ chiến dịch digital marketing hay quản trị nội dung nào. Hiểu cách SEO và SEM hoạt động và ảnh hưởng đến mục tiêu chung sẽ giúp bạn phối hợp tốt hơn với những mảng còn lại của team digital mà không cảm thấy lạc lõng hay ở nhầm chỗ.</p>
            </div>
            <div class="most-view">
                <div class="title_blog-recent">
                    <h3>Xem Nhiều Nhất</h3>
                </div>
                <div class="items_blog-recent">
                    <div class="number_blog">
                        <p>01</p>
                    </div>
                    <div class="content_blog">
                      <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                      <span>5 phút để đọc</span>
                    </div>
                </div>
                <div class="items_blog-recent">
                  <div class="number_blog">
                      <p>02</p>
                  </div>
                  <div class="content_blog">
                    <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                    <span>5 phút để đọc</span>
                  </div>
                </div>
                <div class="items_blog-recent">
                  <div class="number_blog">
                      <p>03</p>
                  </div>
                  <div class="content_blog">
                    <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                    <span>5 phút để đọc</span>
                  </div>
                </div>
                <div class="items_blog-recent">
                <div class="number_blog">
                    <p>04</p>
                </div>
                <div class="content_blog">
                  <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
                  <span>5 phút để đọc</span>
                </div>
                </div>
                <div class="items_blog-recent">
            <div class="number_blog">
                <p>05</p>
            </div>
            <div class="content_blog">
              <p >Digital Marketing là làm gì? Top những kỹ năng Digital Marketer phải biết</p>
              <span>5 phút để đọc</span>
            </div>
                </div>
            </div>
        </div>
    </section>
@endsection