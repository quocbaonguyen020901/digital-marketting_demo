@extends('frontend.layout.app')

@section('style')
<link rel="stylesheet" href="{{ ('assets/contact/contact.css?v=' . time()) }}">

@endsection

@section('content')
    <section class="example contact-us_form">
        <div class="container">
            <div class="title-contact-us">
                <h3>Mocha Ads đồng hành cùng bạn</h3>
                <p>Bạn có câu hỏi dành cho chúng tôi? Đừng ngần ngại! Hãy gửi thông tin cho Mocha Ads<br> và chúng tôi sẽ phản hồi lại bạn sớm nhất</p>
            </div>
            <div class="form-contact-us">
                <div class="contact_form">
                    <div class="contact_site">
                        <div class="form_information">
                            <div class="items_form-infor">
                                <label for="name">Họ và tên</label>
                                <input type="text" placeholder="Họ và tên của bạn là">
                            </div>
                            <div class="items_form-infor flex-form ">
                                <div class="items-flex">
                                    <label for="email">Địa chỉ email</label>
                                    <input type="text" name= "email" placeholder="Địa chỉ Email của bạn là">
                                </div>
                                <div class="items-flex">
                                    <label for="phone" >Số điện thoại</label>
                                    <input type="text" name="phone" placeholder="Số điện thoại của bạn là">
                                </div>
                            </div>
                            <div class="items_form-infor">
                                <label for="detial-request">Đôi lời nhắn gửi</label>
                                <input placeholder="Hãy viết gì đó ..."></input>
                            </div>
                            <div class="button_contact">
                                <button>
                                    <span>Liên hệ ngay</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="contact_infor">
                        <div class="items_infor-contact site">
                            <img src="/img/Group 611.png" alt="#">
                            <div class="text_infor">
                                <p>Tầng 4, toà nhà The Light</p>
                                <p>36 P. Tố Hữu, Trung Văn, Từ Liêm, Hà Nội</p>
                            </div>
                        </div>
                        <div class="items_infor-contact site">
                            <img src="/img/Group 612.png" alt="#">
                            <div class="text_infor">
                                <p>0985 903 900 (Mr Dương)</p>
                                <p>0987 056 607 (Ms Linh)</p>
                            </div>
                        </div>
                        <div class="items_infor-contact">
                            <img src="/img/Group 613.png" alt="#">
                            <div class="text_infor">
                                <p><a href="#">duongtv2@viettel.com.vn</a></p>
                                <p><a href="#">duongtv2@viettel.com.vn</a></p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="example map">
        <div class="container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d245309.6964780575!2d107.55234233045097!3d16.11916267653581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141ea8d2eb81b5d%3A0x96eb6d00b3cbade!2zTmFtIMSQw7RuZywgVGjhu6thIFRoacOqbiBIdeG6vywgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1680598013351!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </section>
@endsection