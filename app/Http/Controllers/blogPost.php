<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class blogPost extends Controller
{
    public function block() {
        return view('frontend.blog-post.blog-post');
    }

    public function index() {
        return view('frontend.home.home');
    }

    public function blog() {
        return view('frontend.blog.blog');
    }

    public function brief() {
        return view('frontend.brief.brief');
    }

    public function contact() {
        return view('frontend.contact.contact');
    
    }
    public function content() {
        return view('frontend.content.content');
    
    }

    public function kol() {
        return view('frontend.kol.kol');
    }

    public function booking() {
        return view('frontend.pr&booking.pr&booking');
    }

    public function tiktok() {
        return view('frontend.tiktok.tiktok');
    }

    public function tiktokAds() {
        return view('frontend.tiktok.tiktok-ads');
    }

    public function tvc() {
        return view('frontend.tvc.tvc');
    }

    public function google() {
        return view('frontend.google.google');
    }

}
