<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::namespace('App\Http\Controllers')->group(function () {
    Route::get('/', 'BlogPost@index' );
    Route::get('/blogPost', 'blogPost@block' );
    Route::get('/blog', 'blog@blog' );
    Route::get('/brief', 'blogPost@brief' );
    Route::get('/contact', 'blogPost@contact' );
    Route::get('/content', 'blogPost@content' );
    Route::get('/google', 'blogPost@google' );
    Route::get('/kol', 'blogPost@kol' );
    Route::get('/booking', 'blogPost@booking');
    Route::get('/tiktok', 'blogPost@tiktok' );
    Route::get('/tiktokAds', 'blogPost@tiktokAds' );
    Route::get('/tvc', 'blogPost@tvc' );
});



